//const API_KEY = '?api_key=RGAPI-5c895868-0d4a-43a9-a87c-c3bdb574fff5';
const API_KEY = "?api_key=RGAPI-baf73a6d-c7b1-47de-a37e-48264bce574b";
//const BASE_URL = 'https://cors-anywhere.herokuapp.com/https://euw1.api.riotgames.com/lol/';
//const BASE_URL = 'https://thingproxy.freeboard.io/fetch/https://euw1.api.riotgames.com/lol/';
const URL = "https://localhost:5001/";
var requestOptions = {
	method: "GET",
	redirect: "follow",
};
class ApiCalls {
	async getSummonerInfo(name, region) {
		return await fetch(URL + "summoner", {
			method: "POST",
			mode: "cors",
			cache: "no-cache",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				region: region,
				summonerName: name,
			}),
		});
	}
	async getSummonerGamesInfo(id, region) {
		return await fetch(URL + "league", {
			method: "POST",
			mode: "cors",
			cache: "no-cache",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				region: region,
				encryptedSummonerId: id,
			}),
		});
	}
	getSummonerGame(summonerId, region) {
		return fetch(URL + "spectator", {
			method: "POST",
			mode: "cors",
			cache: "no-cache",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				region: region,
				encryptedSummonerId: summonerId,
			}),
		});
	}
}
export default new ApiCalls();
