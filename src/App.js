import React, { useState, useEffect } from "react";
import "./App.css";
import Summoners from "./components/Summoners/Summoners";
import ApiCalls from "./functions/api_calls";
import Error from "./components/Error/Error";

function App() {
	const regions = [
		"BR",
		"EUNE",
		"EUW",
		"JP",
		"KR",
		"LA1",
		"LA2",
		"NA",
		"OC",
		"TR",
		"RU",
	];
	const regionsAPI = [
		"br1",
		"eun1",
		"euw1",
		"jp1",
		"kr",
		"la1",
		"la2",
		"na1",
		"oc1",
		"tr1",
		"ru",
	];
	const regionsDict = [];
	regions.forEach((value, index) => {
		let region = {
			region: value,
			regionAPI: regionsAPI[index],
		};
		regionsDict.push(region);
	});

	let [nick, setNick] = useState(localStorage.getItem("nick"));
	let [id, setId] = useState("");
	let [region, setRegion] = useState(localStorage.getItem("region"));
	function displaySummoners() {
		//localStorage.setItem('nick', nick);
		ApiCalls.getSummonerInfo(nick, region)
			.then((res) => res.json())
			.then((json) => {
				if (json.id !== undefined) {
					console.log("id po pobraniu: " + json.id);
					setId("");
					setId(json.id);
					localStorage.setItem("nick", nick);
					localStorage.setItem("region", region);
				}
			});
	}
	const handleSubmit = (event) => {
		event.preventDefault();
	};
	useEffect(() => {
		if (localStorage.getItem("nick") !== null) {
			document.getElementById("summonerName").value =
				localStorage.getItem("nick");
		}
		if (localStorage.getItem("region") !== null) {
			document.getElementById("selectRegion").value =
				localStorage.getItem("region");
		}
	}, []);
	useEffect(() => {
		console.log("id:" + id);
	}, [id]);
	return (
		<div className="App">
			{/* <Summoners id="mJND-cvlCxzANbvOxnmYuYznT76mzaqu-fL1dUBPIn5_BChB" /> */}
			<div className="mt-4 d-flex w-100 justify-content-around">
				<div className="d-flex align-items-center w-25 m-fullwidth justify-content-center">
					<input
						id="summonerName"
						name="nick"
						className="form-control"
						autoComplete={true}
						style={{ width: "auto" }}
						value={nick}
						onChange={(e) => setNick(e.target.value)}
						type="text"
						placeholder="NICK"
					/>
					<select
						className="ml-2 mr-2 form-control w-25 m-20width"
						id="selectRegion"
						onChange={(e) => setRegion(e.currentTarget.value)}
					>
						{regions.map((value, key) => {
							let el = document.createElement("option");
							el.textContent = value;
							el.value = value;
							return (
								<option key={key} value={regionsDict[key].regionAPI}>
									{value}
								</option>
							);
						})}
					</select>
					<button
						className="ml-2 btn btn-primary"
						onClick={() => displaySummoners()}
					>
						<i className="fa fa-refresh" /> GO
					</button>
				</div>
			</div>
			{/* <Summoners id="xIP8QQlnwtkiHrh1DVTxn59oRKCGdHd1H4gdYFdLbLhzlayd" /> */}
			{id === "" ? <Error /> : <Summoners id={id} region={region} />}
		</div>
	);
}

export default App;
