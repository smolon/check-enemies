import React from 'react';

function Error() {
	return <h1 className="text-danger">Wygląda na to, że gracz nie jest w grze</h1>;
}
export default Error;
