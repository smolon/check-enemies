import React, { useEffect, useState } from "react";
import ApiCalls from "../../functions/api_calls";
import styles from "./Summoner.module.css";
function Summoner(props) {
	let [summonerInfo, setSummonerInfo] = useState([]);
	let [summonerGamesInfo, setSummonerGamesInfo] = useState([]);
	let [isLoading, setIsLoading] = useState(true);
	useEffect(() => {
		setIsLoading(true);
		ApiCalls.getSummonerInfo(props.name, props.region)
			.then((response) => response.json())
			.then((result) => {
				setSummonerInfo(result);
				ApiCalls.getSummonerGamesInfo(result.id, props.region)
					.then((response) => response.json())
					.then((result) => {
						if (result[0].queueType === "RANKED_FLEX_SR")
							setSummonerGamesInfo(result[1]);
						else setSummonerGamesInfo(result[0]);
						setIsLoading(false);
					})
					.catch((error) => console.log("error", error));
			})
			.catch((error) => console.log("error", error));
	}, [props.name, props.region]);
	return (
		<div
			style={{
				flexBasis: 20 + "%",
				marginTop: 1 + "em",
				marginBottom: 1 + "em",
			}}
			className={styles.width20}
		>
			{isLoading ? (
				<div className={styles.ldsEllipsis}>
					<div />
					<div />
					<div />
					<div />
				</div>
			) : (
				<div
					className="card mt-3 mb-3"
					style={{
						marginLeft: 3 + "rem",
						marginRight: 3 + "rem",
						backgroundColor: props.color,
					}}
				>
					<div className="card-img-top">
						{typeof summonerGamesInfo === "undefined" ? (
							<h1>Could not fetch emblem</h1>
						) : (
							<img
								alt="ranked emblem"
								src={require(`../../images/ranked-emblems/${summonerGamesInfo.tier}.png`)}
								width="150px"
								height="150px"
							/>
						)}
					</div>
					{typeof summonerGamesInfo === "undefined" ? (
						<h1>Could not fetch data</h1>
					) : (
						<div className="card-body">
							<h4 className="card-title">{props.name}</h4>
							<h5>
								{summonerGamesInfo.tier} {summonerGamesInfo.rank}{" "}
								{summonerGamesInfo.leaguePoints}
							</h5>
							{summonerGamesInfo.miniSeries ? (
								summonerGamesInfo.miniSeries.progress.split("").map((char) => {
									if (char === "W") {
										return "🟢";
									}
									if (char === "N") {
										return "⚫";
									}
									return "🔴";
								})
							) : (
								<span />
							)}
							<h5>Poziom: {summonerInfo.summonerLevel}</h5>
							<h5>
								Rozegrane gry:{" "}
								{summonerGamesInfo.wins + summonerGamesInfo.losses}
							</h5>
							<h6 className="text-success">
								Wygrane: {summonerGamesInfo.wins}
							</h6>
							<h6 className="text-danger">
								Przegrane: {summonerGamesInfo.losses}
							</h6>
							<h6>
								Winratio:{" "}
								{(
									(summonerGamesInfo.wins * 100) /
									(summonerGamesInfo.wins + summonerGamesInfo.losses)
								).toFixed(2)}
								%
							</h6>
							<div className={styles.champStats}>
								<img
									src={props.championIMG}
									alt="champ img"
									width="40px"
									height="40px"
								/>
								{console.log("chamion img url:  " + props.championIMG)}
								<div className="d-flex flex-column ml-2">
									<h6>Zagranych gier: 123</h6>
									<h6>Winratio: 56%</h6>
									{/* w css dodalem dwie klasy - zielona i czerwona, w zaleznosci od wr */}
								</div>
							</div>
						</div>
					)}
				</div>
			)}
		</div>
	);
}

export default Summoner;
