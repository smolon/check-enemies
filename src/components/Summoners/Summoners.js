import React, { useEffect, useState } from "react";
import ApiCalls from "../../functions/api_calls";
import Summoner from "../Summoner/Summoner";
import styles from "../../components/Summoner/Summoner.module.css";
import Error from "../Error/Error";
function Summoners(props) {
	let [gameInfo, setGameInfo] = useState([]);
	let [loaded, setLoaded] = useState(false);
	let [isInGame, setIsInGame] = useState(false);
	let [statusCode, setStatusCode] = useState(404);
	function nameToChampImgUrl(name) {
		return `http://ddragon.leagueoflegends.com/cdn/${championJSON.version}/img/champion/${name}.png`;
	}
	const championJSON = require("../../json/champion.json");
	/*
		narazie niech zostanie ta funkcja, bardzo ciekawa jest
	*/
	// function champions() {
	// 	Object.keys(championJSON.data).forEach((champion) => {
	// 		console.log(championJSON.data[champion]);
	// 	});
	// }
	function getChampNameFromId(id) {
		let champName = "";
		for (let champion in championJSON.data) {
			if (championJSON.data[champion].key === id.toString()) {
				champName = champion;
				break;
			}
		}
		return champName;
	}
	function getCurrentSummonerGame() {
		ApiCalls.getSummonerGame(props.id, props.region)
			.then((res) => res.json())
			.then((json) => {
				setStatusCode(200);
				json.participants.sort();
				setGameInfo(json);
			})
			.catch((error) => {
				console.log(error);
			});
	}
	useEffect(() => {
		getCurrentSummonerGame();
		// eslint-disable-next-line
	}, []);
	useEffect(
		() => {
			getCurrentSummonerGame();
		},
		// eslint-disable-next-line
		[props.id, props.region]
	);
	useEffect(() => {
		setLoaded(true);
		setIsInGame(true);
	}, [gameInfo]);

	let content;
	if (gameInfo.length === 0) {
		content = <Error />;
	} else {
		content = gameInfo.participants.map((player, index) => {
			if (index >= 0 && index < 5)
				return (
					<Summoner
						color="#cce5ff"
						name={player.summonerName}
						key={player.summonerName}
						region={props.region}
						championIMG={nameToChampImgUrl(
							getChampNameFromId(player.championId)
						)}
					/>
				);
			else
				return (
					<Summoner
						color="#f8d7da"
						name={player.summonerName}
						key={player.summonerName}
						region={props.region}
						championIMG={nameToChampImgUrl(
							getChampNameFromId(player.championId)
						)}
					/>
				);
		});
	}
	return (
		<div>
			<div className=" d-none">
				<button
					className="btn btn-primary mt-2"
					value="Odśwież"
					onClick={getCurrentSummonerGame}
				>
					<i className="fa fa-refresh" /> Odśwież
				</button>
			</div>
			<div className="d-flex flex-wrap ml-5 mr-5 justify-content-center">
				{loaded ? (
					content
				) : (
					<div className={styles.ldsEllipsis}>
						<div />
						<div />
						<div />
						<div />
					</div>
				)}
			</div>
		</div>
	);
}

export default Summoners;
